# README #

#Autograde#

 Aplicação web geradora de grades horárias para escolas de ensino fundamental e médio. O objetivo final é verificar como a heurística aplicada se comporta para uma instância simples do clássico problema conhecido como school timetabling problem.

##Features##
Cadastro para cada uma das dimensões formadoras de uma grade, são elas:
* Professores
* Disciplinas
* Ligações entre um professor e uma disciplina (Slot).
* Busca a solução com o mínimo possível de conflitos em um espaço de tempo aceitável.

##Configurações##

* Número de períodos por dia

##Convenções##

* Semana com 5 dias letivos

##Estratégias de busca##

* Heurística Simulated Annealing
* Decaimento de Boltzman 

##Utilização##
Para windows em modo desenvolvimento com ruby on rails instalado,
Navegue até onde está o diretório da aplicação.
ligue o servidor:

rails server


Para testar o estado atual da aplicação no navegador digite na barra de endereço:

localhost:3000/home
