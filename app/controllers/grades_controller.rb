class GradesController < ApplicationController
  include Math
  $volta = 0
  # GET /grades
  # GET /grades.json
  def index
    @grades = Grade.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @grades }
    end
  end

  # GET /grades/1
  # GET /grades/1.json
  def show
    @grade = Grade.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @grade }
    end
  end

  # GET /grades/new
  # GET /grades/new.json
  def new
    @grade = Grade.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @grade }
    end
  end

  # GET /grades/1/edit
  def edit(params)
    @grade = Grade.find(params[:id])
  end

  # POST /grades
  # POST /grades.json
  def create
    #nova grade
    #captura os dados da nova grade
 #   if $volta == 0
    #  end
    @grade = Grade.new(params[:grade])
    $dimensao = (@grade.num_periodos_dia.inspect.to_i )* 5

    respond_to do |format|
      if @grade.save
        format.html { redirect_to @grade, notice: 'A grade final ficou assim' }
        format.json { render json: @grade, status: :created, location: @grade }
      else
        format.html { render action: "new" }
        format.json { render json: @grade.errors, status: :unprocessable_entity }
      end
    end

    ### Preparando estruturas necessárias para a elaboração da grade
    cria_grade_inicial
  end

  def cria_grade_inicial

    max_disciplinas =[]

    disciplinas_atuais = Disciplina.all

    slots_sozinhos = Slot.all

    slot_com_disciplina = []

    turmas = Turma.all

    #Pra inserir um contador de cada disciplina precisa um vetor que conte se já houveram 25 atribuições da disciplina

  while slot_com_disciplina.size < ($dimensao)*(turmas.count)
    slots_sozinhos.each{|cada_slot|
      disciplinas_atuais.each{|cada_disciplina|
        if cada_slot.disciplina_id == cada_disciplina.id
          repete = cada_disciplina.fator
          repete.times{
              slot_com_disciplina << Slot.find_by_disciplina_id(cada_disciplina).id
              #max_disciplinas.delete_at(max_disciplinas.index(ids_disciplinas))
              #ids_disciplinas =  max_disciplinas.sample
          }
        end
      }
    }
  end
    manda_para_SA(slot_com_disciplina)
  end

  public
  def cria_horarios_da_grade(conteudo,custo)

    $volta +=1

    k = 0
    cont = 0
    ds = 0 # dia da semana
    turmas = Turma.all
    hora_periodo = 0
    $feiras = %w{segunda terca quarta quinta sexta}

    num_periodos_dia = @grade.num_periodos_dia.inspect.to_i

    conteudo.each{|slot_da_vez|

      if hora_periodo < num_periodos_dia
        hora_periodo += 1
      else
        hora_periodo = 1
      end
      unless conteudo.nil?
        horario = Horario.create([
                                     dia_semana: $feiras[ds],
                                     grade_id: @grade.id,
                                     slot_id: slot_da_vez,
                                     horario_inicio: hora_periodo,
                                 #      turma_id:turmas[k].id
                                 ])
        cont +=1
        # Controle dos dias da semana
        if hora_periodo == num_periodos_dia
          ds += 1
        end
        if ds > 4
          ds = 0
        end
        # Controle do nome das turmas
        if cont == $dimensao
          k += 1
          cont = 0
        end
      end

      if  conteudo.nil?
        horario = Horario.create([
                                     dia_semana: $feira[ds],
                                     grade_id: @grade.id,
                                     horario_inicio: hora_periodo,
                                     turma_id:turmas[k].id,
                                     slot_id: nil
                                 ])
        cont +=1
        # Controle dos dias da semana
        if hora_periodo == num_periodos_dia
          ds += 1
        end
        if ds > 4
          ds = 0
        end
        # Controle do nome das turmas
        if cont == $dimensao
          k += 1
          cont = 0
        end

      end
    }

    @grade.custo = custo
=begin
    if $volta == 1
      create
    else
      mostra_grade
    end
=end
  end

public
=begin
 def prepara_grade_para_SA

 # h = Horario.all(:conditions=>["grade_id = ?",@grade.id],:select=>"horario_inicio, dia_semana, turma_id")

    vetor_horarios = []
    vetor_interno = []
     #@grade.horarios.each{|horarios_da_grade|
       $feiras.each{|feira|

         todos_periodos = @grade.num_periodos_dia.inspect.to_i
         todos_periodos.times { |periodo|

             periodo +=1

           conflito1 = Horario.all(:conditions=>["horario_inicio = ? AND dia_semana = ? AND grade_id = ?",
                                                  periodo,feira,@grade.id],:select=>"slot_id")

         valores_dos_slots_id = []

         conflito1.each {|valores|

           valores_dos_slots_id << valores.slot_id

         }
         valores_dos_slots_id.each{|id_slot|
         conflito2 = Slot.all(:conditions=>["id = ?",id_slot],:select=>"professor_id")
         conflito2.each{|periodo|
                  if periodo.professor_id != nil
                      vetor_interno << periodo.professor_id
                  else
                      vetor_interno << 'vago'
                  end
              }
               if vetor_interno.size == conflito1.size
                 vetor_horarios << vetor_interno
                 vetor_interno = []
               end
         }
       }
     }
       manda_para_SA(vetor_horarios)
end
=end


 def manda_para_SA(vetor_horarios)

   vetor_horarios = vetor_horarios

   tam = vetor_horarios.size

   simulated_annealing(vetor_horarios)

 end
  # DAQUI EM DIANTE ESTÃO MÉTODOS USADOS PELO PROCESSO DE BUSCA DE SOLUÇÃO
  def calcula_boltzman(max_temp,t)
    boltz = max_temp/Math.log(t)
  end

  def cost(permutation)#CUSTO

    #Altera as posicoes e retorna a distancia atual
    custo =0
    permutation.each do |vi|#pra cada turma em determinado horário
      vi_set = vi.uniq
      custo += (vi.size - vi_set.size)
    end
    return custo
  end

 # Modifica horários por conflitos
  def stochastic_two_opt!(perm)
    tmp = perm #transpose
    t1, t2 = rand(tmp.size), rand(tmp.size)
    t1, t2 = t2, t1 if t1 > t2
    conflitos = []
    tmp[t1...t2].transpose.each_with_index { |vi, i|
      if vi.size > vi.uniq.size
        conflitos << i
      end
    }
    tmp[t1...t2].each{|turma|
      # n = rand(turma.size)
      # puts "Conflitos: #{conflitos}"
      conflitos.each{ |c1|
        # puts "c1: #{c1}"
        # c1, c2 = rand(turma.size), rand(turma.size)
        c2 = rand(turma.size)
        c2 = rand(turma.size) while c2 == c1
        # c1, c2 = c2, c1 if c2 < c1
        # turma[c1..c2] = turma[c1..c2].reverse
        turma[c1], turma[c2] = turma[c2], turma[c1]
      }
    }
   # perm = tmp.transpose
    tmp
  end


  # Aleatório um horário por vez
 def stochastic_two_opt_v2!(perm)
    tmp = perm
    t1, t2 = rand(tmp.size), rand(tmp.size)
    t1, t2 = t2, t1 if t1 > t2
    tmp[t1...t2].transpose.each{|turma|
      n = rand(turma.size)
      n.times{
        c1, c2 = rand(turma.size), rand(turma.size)
        c2 = rand(turma.size) while c2 == c1
        c1, c2 = c2, c1 if c2 < c1
        # turma[c1..c2] = turma[c1..c2].reverse
        turma[c1], turma[c2] = turma[c2], turma[c1]
      }
    }
 tmp
  end


# Aleatório um intervalo de horários por vez
  def stochastic_two_opt_v3!(perm)
    tmp = perm
    t1, t2 = rand(tmp.size), rand(tmp.size)
    t1, t2 = t2, t1 if t1 > t2
    tmp[t1...t2].transpose.each{|turma|
      c1, c2 = rand(turma.size), rand(turma.size)
      c2 = rand(turma.size) while c2 == c1
      c1, c2 = c2, c1 if c2 < c1
      turma[c1..c2] = turma[c1..c2].reverse
    }
   # perm = tmp.transpose
    tmp
  end


  def create_neighbor(current) #CRIA VIZINHANÇA
    candidate = {} #cria uma Hash
    candidate[:vector] = Array.new(current[:vector]) #candidate  recebe um vetor de current
    if $count_s < 10000
       candidate[:vector] = stochastic_two_opt!(candidate[:vector])
    else
      candidate[:vector] = stochastic_two_opt2!(candidate[:vector])
      $count_s = 0
    end
       candidate[:cost] = cost(candidate[:vector].transpose) #manda candidate calc o custo

    return candidate #retorna candidate
  end

  def should_accept?(candidate, current,best,temp,t)#DEVO ACEITAR?

    # return true if candidate[:cost] <= current[:cost] #retorna verdadeiro custo do candidato for menor do que o corrente
    diferenca = current[:cost] - candidate[:cost]
   # puts "diferenca: #{diferenca}"
    ret = 1/(1 + Math.exp(-(diferenca) / temp)) > rand() #pega o delta entre o custo de current e candidate dividido pela temperatura
    #puts "Pior: #{ret}" if ret
    return ret
=begin
    # e usa como expoente do neperiano retorna somente se for maior que um aleatório
    ret = gaussian_distribution(candidate,best,temp,t)
    puts "Pior: #{ret}" if ret
    return]

=end
  end

  def search(grade, max_iter, max_temp) #BUSCA
    ultima_atual = 0
    current = {:vector=>grade}#cria uma Hash com :vector chama o método permutação aleatória manda cities , guarda o resultado na hash
    #calcula o custo dessa primeira configuração
    current[:cost] = cost(current[:vector])
    best = current
    temp = max_temp
    max_iter.times do |iter| #repete max_times vezes
      candidate = create_neighbor(current)#cria vizinhança para current usando dados de cities
      if candidate[:cost] < current[:cost] or should_accept?(candidate, current, best, temp, iter+1)
        current = candidate
        if current[:cost] < best[:cost]
          best = current #best recebe candidate se o custo dele for menor que best
          ultima_atual = iter
          $count_s = 0
          #puts "Contando: #{$count_s}"
        end
      end
      if (iter+1).modulo(10) == 0 #Se o resto da divisão  de cada iteração+1 por 10 for igual a 0
       # puts " > iteration #{(iter+1)}, temp=#{temp}, best=#{best[:cost] }, atualizada em #{ultima_atual}" #imprima iter+1 a temperatura usada e o custo da soluçao
      end
      temp = calcula_boltzman(max_temp, iter+2) #temp recebe a nova temperatura reduzida
    end
    return best
  end

  def simulated_annealing(grade)

    slots_grade = grade.each_slice(25).to_a
    # algorithm configuration
    max_iterations = 55000
    #maximo de iteracoes
    max_temp = 100e12
    $count_s = 0
    #temperatura maxima
    # execute the algorithm
    best = search(slots_grade, max_iterations, max_temp)
    puts "Done. Best Solution: c=#{best[:cost]}"
 #   puts "v=#{best[:vector].inspect}"
    grade_final = best[:vector].flatten
    grade_final_custo = best[:cost].inspect.to_s

  cria_horarios_da_grade(grade_final,grade_final_custo)

  end

  # PUT /grades/1
  # PUT /grades/1.json
  def update
    @grade = Grade.find( params[:id] )
    @grade.update_attributes!( params[:horarios] )
    respond_to do |format|
      format.js
    end
  end

  # DELETE /grades/1
  # DELETE /grades/1.json
  def destroy
    @grade = Grade.find(params[:id])
    @grade.destroy

    respond_to do |format|
      format.html { redirect_to grades_url }
      format.json { head :no_content }
    end
  end
  def mostra_grade

  end
end


