class GradeRestricosController < ApplicationController
  # GET /grade_restricos
  # GET /grade_restricos.json
  def index
    @grade_restricos = GradeRestrico.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @grade_restricos }
    end
  end

  # GET /grade_restricos/1
  # GET /grade_restricos/1.json
  def show
    @grade_restrico = GradeRestrico.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @grade_restrico }
    end
  end

  # GET /grade_restricos/new
  # GET /grade_restricos/new.json
  def new
    @grade_restrico = GradeRestrico.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @grade_restrico }
    end
  end

  # GET /grade_restricos/1/edit
  def edit
    @grade_restrico = GradeRestrico.find(params[:id])
  end

  # POST /grade_restricos
  # POST /grade_restricos.json
  def create
    @grade_restrico = GradeRestrico.new(params[:grade_restrico])

    respond_to do |format|
      if @grade_restrico.save
        format.html { redirect_to @grade_restrico, notice: 'Grade restrico was successfully created.' }
        format.json { render json: @grade_restrico, status: :created, location: @grade_restrico }
      else
        format.html { render action: "new" }
        format.json { render json: @grade_restrico.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /grade_restricos/1
  # PUT /grade_restricos/1.json
  def update
    @grade_restrico = GradeRestrico.find(params[:id])

    respond_to do |format|
      if @grade_restrico.update_attributes(params[:grade_restrico])
        format.html { redirect_to @grade_restrico, notice: 'Grade restrico was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @grade_restrico.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grade_restricos/1
  # DELETE /grade_restricos/1.json
  def destroy
    @grade_restrico = GradeRestrico.find(params[:id])
    @grade_restrico.destroy

    respond_to do |format|
      format.html { redirect_to grade_restricos_url }
      format.json { head :no_content }
    end
  end
end
