class Simulated_annealing < ApplicationController


   def cost(permutation)#CUSTO - recebe um vetor e cities
     #Altera as posicoes e retorna a distancia atual
     custo =0
     permutation = permutation
     permutation.each do |vi|#pra cada turma em determinado horário
       vi_set = vi.uniq
       custo += (vi.size - vi_set.size)
     end
     return custo
   end

=begin
  def stochastic_two_opt!(perm)
    tmp = perm.transpose
    t1, t2 = rand(tmp.size), rand(tmp.size)
    t1, t2 = t2, t1 if t1 > t2
    conflitos = []
    tmp[t1...t2].transpose.each_with_index { |vi, i|
      if vi.size > vi.uniq.size
        conflitos << i
      end
    }
    tmp[t1...t2].each{|turma|
      # n = rand(turma.size)
      # puts "Conflitos: #{conflitos}"
      conflitos.each{ |c1|
        # puts "c1: #{c1}"
        # c1, c2 = rand(turma.size), rand(turma.size)
        c2 = rand(turma.size)
        c2 = rand(turma.size) while c2 == c1
        # c1, c2 = c2, c1 if c2 < c1
        # turma[c1..c2] = turma[c1..c2].reverse
        turma[c1], turma[c2] = turma[c2], turma[c1]
      }
    }
    perm = tmp.transpose
  end


 def stochastic_two_opt!(perm)
    tmp = perm.transpose
    t1, t2 = rand(tmp.size), rand(tmp.size)
    t1, t2 = t2, t1 if t1 > t2
    tmp[t1...t2].each{|turma|
      n = rand(turma.size)
      n.times{
        c1, c2 = rand(turma.size), rand(turma.size)
        c2 = rand(turma.size) while c2 == c1
        c1, c2 = c2, c1 if c2 < c1
        # turma[c1..c2] = turma[c1..c2].reverse
        turma[c1], turma[c2] = turma[c2], turma[c1]
      }
    }
    perm = tmp.transpose
  end
=end

  def stochastic_two_opt!(perm)
    perm = perm
    tmp = perm.transpose
    t1, t2 = rand(tmp.size), rand(tmp.size)
    t1, t2 = t2, t1 if t1 > t2
    tmp[t1...t2].each{|turma|
      c1, c2 = rand(turma.size), rand(turma.size)
      c2 = rand(turma.size) while c2 == c1
      c1, c2 = c2, c1 if c2 < c1
      turma[c1..c2] = turma[c1..c2].reverse
    }
    perm = tmp.transpose
  end


   def create_neighbor(current) #CRIA VIZINHANÇA
     candidate = {} #cria uma Hash
     candidate[:vector] = Array.new(current[:vector]) #candidate  recebe um vetor de current
     candidate[:vector] = stochastic_two_opt!(candidate[:vector])#passa candidate para o metodo
     candidate[:cost] = cost(candidate[:vector]) #manda candidate e cities para calcular o custo
     return candidate #retorna candidate
   end

   def should_accept?(candidate, current, temp)#DEVO ACEITAR?
=begin
     return true if candidate[:cost] <= current[:cost] #retorna verdadeiro custo do candidato for menor do que o corrente
     ret = Math.exp((current[:cost] - candidate[:cost]) / temp) > rand() #pega o delta entre o custo de current e candidate dividido pela temperatura
     puts "Pior: #{ret}" if ret
     return ret
     # e usa como expoente do neperiano retorna somente se for maior que um aleatório
=end
     d = 2
     ret = gaussian_distribution(temp,d)

   end

   def search(grade, max_iter, max_temp, temp_change, t) #BUSCA
     ultima_atual = 0
     current = {:vector=>grade}#cria uma Hash com :vector chama o método permutação aleatória manda cities , guarda o resultado na hash
     #calcula o custo dessa primeira configuração
     current[:cost] = cost(current[:vector])
     temp, best = max_temp, current #guarda max_temp dentro de temp e current em best
     max_iter.times do |iter| #repete max_times vezes
       candidate = create_neighbor(current)#cria vizinhança para current usando dados de cities
       puts candidate[:cost]
       temp = temp * temp_change #temp recebe a nova temperatura reduzida
       current = candidate if should_accept?(candidate, current, temp)#current recebe candidate se o método devo aceitar? disser que sim
       if candidate[:cost] < best[:cost]
          best = candidate #best recebe candidate se o custo dele for menor que best
          ultima_atual = iter
       end
       if (iter+1).modulo(10) == 0 #Se o resto da divisão  de cada iteração+1 por 10 for igual a 0
         puts " > iteration #{(iter+1)}, temp=#{temp}, best=#{best[:cost] }, atualizada em #{ultima_atual}" #imprima iter+1 a temperatura usada e o custo da soluçao
       end
       t+=1
       temp_change = calcula_boltzman(max_temp,t)
     end
     return best, t
   end
   def cauchy_distribution()

   end
   def gaussian_distribution(temperatura,d)
     deltax = candidate[:cost]- best[:cost]
     exp_= Math.exp((-deltax**2)/2*temperatura)
     gBoltz_denominador = (2*PI*temperatura)**(d/2) #VERIFICAR A DIMENSIONALIDADE DO PROBLEMA
     gBoltz = (1/gBoltz_denominador)*exp_
   end
   def calcula_boltzman(max_temp,t)

     boltz = max_temp/log(t)
     end
  def initialize(grade)

    grade_inicial = grade
    # algorithm configuration
    max_iterations = 10
       #maximo de iteracoes
    max_temp = 1e200
    t=1
     #temperatura maxima
    temp_change = calcula_boltzman(max_temp,t)#1.3806503e-16 # passo de decremento da temperatura
    # execute the algorithm
    best = search(grade, max_iterations, max_temp, temp_change,t)
    puts "Done. Best Solution: c=#{best[:cost]}"
    puts "v=#{best[:vector].inspect}"
    grade_final = best[:vector].to_enum
   # @grade[:custo] = best[:cost]

    slots_da_grade_final =[]

     grade_final.each { |result|

        result.each{|professor_ids|

          slot_do_prof = Slot.all(:conditions=>["professor_id = ?",professor_ids],:select=>"id")

          slot_do_prof.each{|slot_id|

            slots_da_grade_final << slot_id

          }
        }
     }

    return slots_da_grade_final
  # redirect_to :controller=> "grades_controller", :action=> "cria_horarios_grade", :argument => slots_da_grade_final
  # nova = Grades_Controller.new
   #nova.cria_horarios_da_grade(slots_da_grade_final)
  end
end

