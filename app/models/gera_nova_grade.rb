class GeraNovaGrade


       if __FILE__ == $0

         # configuracao do problema
         grade = gera_grade_inicial
         avaliacao = avalia_horario grade
         # parametros iniciais do algoritmo
         max_iterations = 1000
         max_temp = 100000.0
         temp_change = 0.98
         # chamada de execucao
         best = busca(grade, max_iterations, max_temp, temp_change)

         puts "Terminado.\n Custo da melhor grade: c=#{best[:cost]}\n GRADE FINAL:\n#{best[:solution].inspect}\t\t\n"

            File.open('Grade de horario - v3', 'w'){ |f|


              best[:solution].each_with_index{|s,idx|
                f.write  "\n #{$nomes_turmas[idx]}: #{s.inspect}\n"
              }
            }

    end
  end

    def avalia_horario(grade)

        custo = 0
        #Itera dentro das grades de restricao de cada professor em busca de matches
        #entre restricoes fortes e atribuicoes de aulas do mesmo aquele horario

        $professores.each { |professor,restricoes|
            disciplina = $atribuicoes[professor]

            grade.each { |turma|
                turma.each_index { |i|

                    aula = turma[i]
                    if aula == disciplina
                        restricao = restricoes[i]
                             if restricao == 3
                            custo += 25
                        elsif restricao == 2
                            custo += 18
                        elsif restricao == 1
                            custo += 10
                        else
                            custo -= 3
                        end
                    end
                }
            }
        }
   end


    def stochastic_two_opt!(perm)
      c1, c2 = rand(perm.size), rand(perm.size)
      exclude = [c1]
      exclude << ((c1==0) ? perm.size-1 : c1-1)
      exclude << ((c1==perm.size-1) ? 0 : c1+1)
      c2 = rand(perm.size) while exclude.include?(c2)
      c1, c2 = c2, c1 if c2 < c1

      perm[c1...c2] = perm[c1...c2].reverse
      return perm
    end

    def perturbacao_estocastica!(matrix)
        matrix.each { |vector|
            stochastic_two_opt!(vector)
        }
        return matrix
    end

    def cria_vizinho(current)
      candidate = {}
      candidate[:solution] = Array.new(current[:solution])
      perturbacao_estocastica!(candidate[:solution])
      candidate[:cost] = avalia_horario(candidate[:solution])
      return candidate
    end

    def devo_aceitar?(candidate, current, temp)
      return true if candidate[:cost] <= current[:cost]
      return Math.exp((current[:cost] - candidate[:cost]) / temp) > rand()
    end



    def busca(grade, max_iter, max_temp, temp_change)
      current = {:solution=>grade}
      current[:cost] = avalia_horario(current[:solution])
      temp, best = max_temp, current
      max_iter.times do |iter|
        candidate = cria_vizinho(current)
       # ap candidate #imprime cadidate
        temp = temp * temp_change #muda a temperatura
        current = candidate if devo_aceitar?(candidate, current, temp)
        best = candidate if candidate[:cost] < best[:cost] #melhor passa a ser a de menor custo dentre as testadas
        if (iter+1).modulo(10) == 0
          puts " > iteracoes #{(iter+1)}, temperatura=#{temp}, menor custo=#{best[:cost]}"
        end
        end
      return best
    end

=begin
      #Efetua a verificação vertical em cima da estrutura $periodo, em busca de choques verticais
          ult = 0
           2.times{|primeiro|
             if ult==1 # esse 1 precisa ser variável pois para cada turma o primeiro item vai variar
                @vertical << @periodos[primeiro]
             end

             ult = ult + 1
             }

            @periodos.each{|aulavert|
               todos_horarios = Horario.all
               total_todos_horarios = todos_horarios.count

               ct = ct + 1
               # Define o pulo entre
               multiplo = (ct%total_todos_horarios)
               # Se houver uma divisao exata entre o
                  if multiplo == 0
                     @vertical << aulavert
                  end
            }

            @vertical = @vertical.uniq

            render(:template=>processa_grade)
      end


end


=begin
            File.open('Grade de horario - v3', 'w'){ |f|
                f.write  "\n Grade criada: #{periodo}"
                f.write  "\n Verticalizando :#{vertical}\n"
                f.write  " \nE agora sem repties #{evita_choq_prof}\n"
            }

            # Parte do final algoritmo
            #$nova_grade = Grade.new(periodo) #Cria um novo objeto grade.

=end
=begin
def initialize(horarios, disciplinas)

  @horarios_copia = horarios

  @disciplinas_copia = disciplinas

  @grade_gerada =[nil]

  @grade_gerada_na_vertical = []

  ct = 0

  @max_disciplinas =[]

  # Busca todos os horarios já definidos
  #@horarios = Horario.all
  #@horarios_copia = @horarios.dup


  # Define a dimensao da Grade
  # @verifica_tamanho_grade = Grade.find(params[:id])
  # @periodos_por_dia = @verifica_tamanho_grade.num_periodos_dia
  # @max_slots_semana = @periodos_por_dia * @dias_da_semana.size

  # Obtem os dados das disciplinas considerando o fator maximo semanal
  #@disciplinas = Disciplina.all
  #@disciplinas_copia = @disciplinas.dup

  # Constroi a estrutura auxiliar para controle do
  # numero de atribuiçoes segundo disciplinas definidas
  @disciplinas_copia.each{|materia|
    fator = materia.fator
    fator.times do
      @max_disciplinas << materia.id # recebe o ID da materia o numero de vezes o seu peso
    end
  }

  # @sobra = @max_slots_semana - @periodos.size #Usado para atribuição de espaços em branco

  @max_disciplinas_copia = @max_disciplinas.dup
  #Compara as disciplinas dentro dos slots conectados aos horarios com a estrutura logo acima

  #Testa se ainda existem IDs dentro da max disciplinas a serem atribuidos
  until @max_disciplinas_copia.empty?

    @max_disciplinas_copia.each{|disciplina|

      # Efetua as operacoes na estrutura de max disciplinas
      delete_list=[]
      aula = @max_disciplinas_copia.sample # picked up an item from array
      @grade_gerada << aula
      delete_list << aula
      delete_list.each do |del|
        @max_disciplinas_copia.delete_at(@max_disciplinas_copia.index(del))
      end
    }

  end
  @grade_gerada.each do |ids_disciplinas|
    @todos_slots_slot = Slot.find_all_by_disciplina_id(ids_disciplinas)
    @todos_slots
  end
end
end
=end