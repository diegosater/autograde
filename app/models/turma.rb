# == Schema Information
#
# Table name: turmas
#
#  id         :integer          not null, primary key
#  nome_turma :string(255)
#  grade_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Turma < ActiveRecord::Base
  attr_accessible :grade_id, :nome_turma

  has_one :Grade
end
