# == Schema Information
#
# Table name: horarios
#
#  id             :integer          not null, primary key
#  grade_id       :integer
#  dia_semana     :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  horario_inicio :integer
#  slot_id        :integer
#  turma_id       :integer
#

class Horario < ActiveRecord::Base

  attr_accessible  :dia_semana, :grade_id, :horario_inicio, :slot_id, :turma_id

  belongs_to :grade

 # belongs_to :grade_restrico

  belongs_to :slot

  # validates_presence_of :slot

end

