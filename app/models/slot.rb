# == Schema Information
#
# Table name: slots
#
#  id            :integer          not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  horario_id    :integer
#  disciplina_id :integer
#  professor_id  :integer
#

class Slot < ActiveRecord::Base

  attr_accessible :disciplina_id ,:professor_id, :horario_id

  has_many :horario

  has_one :professor,
  		    :class_name => "Professor",
  		    :foreign_key => 'id',
          :dependent => :destroy

  has_one :disciplina,
  		    :class_name => "Disciplina",
  		    :foreign_key => 'id',
          :dependent => :destroy

  #validates_presence =>:professor_id, :disciplina_id


  #validates_presence_of :disciplina, :professor

  after_create :atualiza_slot_id_quando_criado_agendamento

  def atualiza_slot_id_quando_criado_agendamento

    p = Professor.find_by_id(self.professor_id)
    p.slot_id = self.id
  end


end
