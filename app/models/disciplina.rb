# == Schema Information
#
# Table name: disciplinas
#
#  id              :integer          not null, primary key
#  nome_disciplina :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  fator           :integer
#  slot_id         :integer
#

class Disciplina < ActiveRecord::Base
  attr_accessible :nome_disciplina,:fator, :slot_id

  belongs_to :slot

 end
