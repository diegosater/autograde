# == Schema Information
#
# Table name: grades
#
#  id                     :integer          not null, primary key
#  num_periodos_dia       :integer          default(5)
#  tempo_cada_periodo     :time
#  turma_id               :string(255)      default("generica")
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  hora_de_inicio_da_aula :time
#  score_solucao          :integer
#

class GradeRestrico <Grade #< ActiveRecord::Base

  attr_accessible :nome_professor_id, :vetor_restricoes

  belongs_to :professor

  @vetor_restricoes = []
=begin
  def initialize(professor)
  	:nome_professor = professor || 'anonimo'
  end
=end
end
