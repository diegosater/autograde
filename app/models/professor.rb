# == Schema Information
#
# Table name: professors
#
#  id                  :integer          not null, primary key
#  nome_professor      :string(255)
#  grade_restricoes_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  slot_id             :integer
#

class Professor < ActiveRecord::Base

  attr_accessible :nome_professor, :slot_id

  belongs_to :slot

end
