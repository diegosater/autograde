require 'test_helper'

class GradeRestricosControllerTest < ActionController::TestCase
  setup do
    @grade_restrico = grade_restricos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:grade_restricos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create grade_restrico" do
    assert_difference('GradeRestrico.count') do
      post :create, grade_restrico: { nome_professor: @grade_restrico.nome_professor }
    end

    assert_redirected_to grade_restrico_path(assigns(:grade_restrico))
  end

  test "should show grade_restrico" do
    get :show, id: @grade_restrico
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @grade_restrico
    assert_response :success
  end

  test "should update grade_restrico" do
    put :update, id: @grade_restrico, grade_restrico: { nome_professor: @grade_restrico.nome_professor }
    assert_redirected_to grade_restrico_path(assigns(:grade_restrico))
  end

  test "should destroy grade_restrico" do
    assert_difference('GradeRestrico.count', -1) do
      delete :destroy, id: @grade_restrico
    end

    assert_redirected_to grade_restricos_path
  end
end
