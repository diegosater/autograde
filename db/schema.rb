# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150411124023) do

  create_table "disciplinas", :force => true do |t|
    t.string   "nome_disciplina"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "fator"
    t.integer  "slot_id"
  end

  create_table "grade_restricos", :force => true do |t|
    t.string   "nome_professor"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "grades", :force => true do |t|
    t.integer  "num_periodos_dia",                      :default => 5
    t.integer  "turma_id",               :limit => 255, :default => 0
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.time     "hora_de_inicio_da_aula"
    t.string   "nome_grade"
    t.string   "custo"
  end

  create_table "horarios", :force => true do |t|
    t.integer  "grade_id"
    t.string   "dia_semana"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "horario_inicio"
    t.integer  "slot_id"
    t.integer  "turma_id",       :limit => 255
  end

  create_table "professors", :force => true do |t|
    t.string   "nome_professor"
    t.integer  "grade_restricoes_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "slot_id"
  end

  create_table "slots", :force => true do |t|
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "horario_id"
    t.integer  "disciplina_id"
    t.integer  "professor_id"
  end

  create_table "turmas", :force => true do |t|
    t.string   "nome_turma"
    t.integer  "grade_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "usuarios", :force => true do |t|
    t.string   "nome"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
