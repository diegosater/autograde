class CreateHorarios < ActiveRecord::Migration
  def change
    create_table :horarios do |t|
      t.datetime :dia_e_hora
      t.integer :Slot
      t.foreign_key :slot
      t.timestamps
    end
  end
end
