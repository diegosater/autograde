class AjustaModelGrade < ActiveRecord::Migration
  def up
    remove_column :grades, :score_solucao, :integer
    remove_column :grades, :tempo_cada_periodo, :integer

  end

  def down
    add_column :grades, :score_solucao, :integer
    add_column :grades, :tempo_cada_periodo, :integer
  end
end
