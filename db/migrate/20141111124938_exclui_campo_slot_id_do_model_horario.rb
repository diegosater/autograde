class ExcluiCampoSlotIdDoModelHorario < ActiveRecord::Migration
  def up
    remove_column :horarios, :slot_id, :integer
  end

  def down
    add_column :horarios, :slot_id, :integer
  end
end
