class CriaCamposSlotIdNosModelsProfessorEDisciplina < ActiveRecord::Migration
  def up
    add_column :disciplinas, :slot_id, :integer
    add_column :professors, :slot_id, :integer
  end

  def down
    remove_column :disciplinas, :slot_id, :integer
    remove_column :professors, :slot_id, :integer
  end
end
