class CreateGradeRestricos < ActiveRecord::Migration
  def change
    create_table :grade_restricos do |t|
      t.string :nome_professor
      t.foreign_key :professor
      t.timestamps
    end
  end
end
