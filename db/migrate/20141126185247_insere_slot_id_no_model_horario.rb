class InsereSlotIdNoModelHorario < ActiveRecord::Migration
  def up
    add_column :horarios, :slot_id, :integer
  end

  def down
    remove_column :horarios, :slot_id, :integer
  end
end
