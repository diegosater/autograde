class ExcluiColunaCustoGradeModel < ActiveRecord::Migration
  def up
    remove_column :grades, :custo, :integer
  end

  def down
    add_column :grades, :custo, :integer
  end
end
