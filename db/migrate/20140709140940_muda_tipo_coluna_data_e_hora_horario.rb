class MudaTipoColunaDataEHoraHorario < ActiveRecord::Migration
  def up
  	change_table :Horarios do |t|
  		t.remove :dia_e_hora
  		t.string :dia_semana
    end
  end

  def down
  end
end
