class CreateHorario < ActiveRecord::Migration
  def change
    create_table :horarios do |t|
      t.integer :slot_id
      t.integer :grade_id
      t.string :dia_semana
      t.timestamps
    end
  end
end
