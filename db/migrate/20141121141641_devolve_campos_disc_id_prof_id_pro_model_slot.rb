class DevolveCamposDiscIdProfIdProModelSlot < ActiveRecord::Migration
  def up
    add_column :slots, :disciplina_id, :integer
    add_column :slots, :professor_id, :integer

  end

  def down
    remove_column :slots, :disciplina_id, :integer
    remove_column :slots, :professor_id, :integer
  end
end
