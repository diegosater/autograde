class CreateTurmas < ActiveRecord::Migration
  def change
    create_table :turmas do |t|
      t.string :nome_turma
      t.integer :grade_id
      t.foreign_key :grade_id

      t.timestamps
    end
  end
end
