class MudaTipoDoCampoHorarioInicioModelHorario < ActiveRecord::Migration
  def up
    change_column :horarios,:horario_inicio,:datetime
  end

  def down
    change_column :horarios, :horario_inicio, :time
  end
end