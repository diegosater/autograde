class CriaCampoHoraDeInicioDaAulaNoModelGrade < ActiveRecord::Migration
  def up
    add_column :grades, :hora_de_inicio_da_aula, :time
  end

  def down
    remove_column :grades, :hora_de_inicio_da_aula, :time
  end
end
