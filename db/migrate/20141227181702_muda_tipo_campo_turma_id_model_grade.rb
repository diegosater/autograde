class MudaTipoCampoTurmaIdModelGrade < ActiveRecord::Migration
  def up
    change_column :grades,:turma_id,:integer
  end

  def down
    change_column :grades, :turma_id, :string
  end
end
