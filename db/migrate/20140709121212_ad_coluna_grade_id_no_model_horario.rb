class AdColunaGradeIdNoModelHorario < ActiveRecord::Migration
  def up
  	add_column :horarios,:grade_id,:integer
  	change_table :Horarios do |t|
  	  t.rename :Slot, :slot_id
    end
  end

  def down
  end
end
