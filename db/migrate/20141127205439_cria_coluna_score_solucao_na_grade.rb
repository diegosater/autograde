class CriaColunaScoreSolucaoNaGrade < ActiveRecord::Migration
  def up
    add_column :grades, :score_solucao, :integer
  end

  def down
    remove_column :grades, :score_solucao, :integer
  end
end
