class CreateGrade < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.integer :num_periodos_dia, :default=>'5'
      t.time :tempo_cada_periodo, :default=>'50'
      t.string :turma, :default=>'generica'

      t.timestamps
    end
  end
end

