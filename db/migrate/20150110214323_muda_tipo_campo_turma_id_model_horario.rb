class MudaTipoCampoTurmaIdModelHorario < ActiveRecord::Migration
  def up
    change_column :horarios,:turma_id,:integer
  end

  def down
    change_column :horarios, :turma_id, :string
  end
end
