class MudaTipoHorarioInicioModelHorario < ActiveRecord::Migration
  def up
    change_column :horarios,:horario_inicio,:integer
  end

  def down
    change_column :horarios, :horario_inicio, :datetime
  end
end
