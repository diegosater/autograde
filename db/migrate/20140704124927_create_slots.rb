class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.column :professor_id, :integer
      t.column :disciplina_id, :integer
      t.foreign_key :professor
      t.foreign_key :disciplina

      t.timestamps
    end
  end
end
