class ExcluiCampoDisciplinaIdEProfessorIdDoModelSlot < ActiveRecord::Migration
  def up
    remove_column :slots, :disciplina_id, :integer
    remove_column :slots, :professor_id, :integer

  end

  def down
    add_column :slots, :disciplina_id, :integer
    add_column :slots, :professor_id, :integer
  end
end
