class ExcluiCampoHorarioIdDoModelGrade < ActiveRecord::Migration
  def up
    remove_column :grades, :horario_id, :integer
  end

  def down
    add_column :grades, :horario_id, :integer
  end
end
