class CreateProfessors < ActiveRecord::Migration
  def change
    create_table :professors do |t|
      t.string :nome_professor
      t.column :grade_restricoes_id, :integer
      t.foreign_key :grade_restrico

      t.timestamps
    end
  end
end
