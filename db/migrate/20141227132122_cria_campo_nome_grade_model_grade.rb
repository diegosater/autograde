class CriaCampoNomeGradeModelGrade < ActiveRecord::Migration
  def up
    add_column :grades, :nome_grade, :string
  end

  def down
    remove_column :grades, :nome_grade, :string
  end
end
