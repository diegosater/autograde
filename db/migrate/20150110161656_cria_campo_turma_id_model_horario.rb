class CriaCampoTurmaIdModelHorario < ActiveRecord::Migration
  def up
    add_column :horarios, :turma_id, :string
  end

  def down
    remove_column :horarios, :turma_id, :string
  end

end
