class AjustaModelDisciplina < ActiveRecord::Migration
  def up
    remove_column :disciplinas, :peso, :integer
  end

  def down
    add_column :disciplinas, :peso, :integer
  end
end
