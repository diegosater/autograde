class CriaCampoCustoNoModelGrade < ActiveRecord::Migration
  def up
    add_column :grades, :custo, :integer
  end

  def down
    remove_column :grades, :custo, :integer
  end
end
