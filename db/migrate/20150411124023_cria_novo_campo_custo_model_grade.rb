class CriaNovoCampoCustoModelGrade < ActiveRecord::Migration
  def up
    add_column :grades, :custo, :string
  end

  def down
    remove_column :grades, :custo, :string
  end
end
